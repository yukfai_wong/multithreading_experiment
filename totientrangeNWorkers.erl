-module(totientrangeNWorkers).
-export([hcf/2,
 	 relprime/2,
	 euler/1,
	 sumTotient/2,
   workerName/1,
   start_server/0,
   server/0,
   server/4,
   totientWorker/0
	]).
totientWorker() ->
  io:format("Worker: Started~n"),
  receive
     {range, Lower, Upper} ->
       io:format("Worker: Computing Range ~p ~p~n", [Lower,Upper]),
       server ! {adding, sumTotient(Lower,Upper)},
       totientWorker();
     finished ->
       io:format("Worker: Finished~n")
  end.

workerName(Num) ->
 list_to_atom( "worker" ++ integer_to_list( Num )).

server() ->
  receive
	    {range, Lower, Upper, Threads} ->
        Worker = [workerName(X) || X <- lists:seq(0,(Threads-1))],
        [register(X ,spawn(totientrangeNWorkers, totientWorker, [])) || X <- Worker],
        Slide = (Upper-Lower+1) div Threads,
        lists:foldl((fun(X, Sum) -> workerName(X) ! {range, ((Slide*Sum)+Lower), (Slide*(Sum+1))}, (Sum + 1) end), 0, lists:seq(0,(Threads-1))),
        server(0, Threads, Worker, os:timestamp())
  end.
server(Result, 0, Worker, {_, S, US}) ->
  io:format("Server: Sum of totients: ~p~n", [Result]),
  lists:foreach(fun (Pid) -> Pid ! finished end, Worker),
  printElapsed(S,US),
  server();
server(Sum, IsWorking, Worker,  Time) ->
  receive
      {adding, Workresult} ->
        io:format("Server: Received Sum ~p~n", [Workresult]),
        server(Sum+Workresult, IsWorking-1, Worker, Time)
  end.

start_server() ->
  register(server, spawn(totientrangeNWorkers, server, [])).


%% hcf x 0 = x
%% hcf x y = hcf y (rem x y)

hcf(X,0) -> X;
hcf(X,Y) -> hcf(Y,X rem Y).

%% relprime x y = hcf x y == 1

relprime(X,Y) ->
  V = hcf(X,Y),
  if
    V == 1
      -> true;
    true
      -> false
  end.

%%euler n = length (filter (relprime n) (mkList n))

euler(N) ->
  RelprimeN = fun(Y) -> relprime(N,Y) end,
  length (lists:filter(RelprimeN,(lists:seq(1,N)))).

%% Take completion timestamp, and print elapsed time

printElapsed(S,US) ->
  {_, S2, US2} = os:timestamp(),
                       %% Adjust Seconds if completion Microsecs > start Microsecs
  if
    US2-US < 0 ->
      S3 = S2-1,
      US3 = US2+1000000;
    true ->
      S3 = S2,
      US3 = US2
  end,
  io:format("Time taken in Secs, MicroSecs ~p ~p~n",[S3-S,US3-US]).


%%sumTotient lower upper = sum (map euler [lower, lower+1 .. upper])

sumTotient(Lower,Upper) ->
  % {_, S, US} = os:timestamp(),
  lists:sum(lists:map(fun euler/1,lists:seq(Lower, Upper))).
  % io:format("Sum of totients: ~p~n", [Res]),
  % printElapsed(S,US).
