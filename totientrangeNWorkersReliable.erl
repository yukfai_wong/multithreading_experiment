-module(totientrangeNWorkersReliable).
-export([hcf/2,
 	 relprime/2,
	 euler/1,
	 sumTotient/2,
   workerName/1,
   start_server/0,
   server/0,
   server/4,
   totientWorker/0,
   watcher/3,
   workerChaos/2,
   testRobust/2
	]).

totientWorker() ->
  receive
     {range, Lower, Upper} ->
       io:format("Worker: Computing Range ~p ~p~n", [Lower,Upper]),
       server ! {adding, sumTotient(Lower,Upper)},
       totientWorker();
     finished ->
       io:format("Worker: Finished~n")
  end.

workerName(Num) ->
 list_to_atom( "worker" ++ integer_to_list( Num )).

server() ->
  receive
    {range, Lower, Upper, Threads} ->
      Worker = [workerName(X) || X <- lists:seq(0,(Threads-1))],
      Slide = (Upper-Lower+1) div Threads,
      lists:foldl(
      fun(X, Sum) ->
        spawn(totientrangeNWorkersReliable, watcher,[X, (Slide*Sum)+Lower, Slide*(Sum+1)]),
        (Sum + 1)
      end, 0, Worker),
      server(0, Threads, Worker, os:timestamp())
  end.

server(Result, 0, Worker, {_, S, US}) ->
  io:format("Server: Sum of totients: ~p~n", [Result]),
  lists:foreach(fun (Pid) -> Pid ! finished end, Worker),
  printElapsed(S,US),
  server();
server(Sum, IsWorking, Worker,  Time) ->
  receive
    {adding, Workresult} ->
      io:format("Server: Received Sum ~p~n", [Workresult]),
      server(Sum+Workresult, IsWorking-1, Worker, Time)
  end.

start_server() ->
  register(server, spawn(totientrangeNWorkersReliable, server, [])).


watcher(Worker, Low, High) ->
  process_flag(trap_exit, true),
  Pid = spawn_link(totientrangeNWorkersReliable, totientWorker, []),
  register(Worker, Pid),
  Worker!{range, Low, High},
  io:format("Watcher: Watching Worker ~p~n",[Worker]),
  receive
    {'EXIT', Pid, normal} ->% not a crash
      ok;
    {'EXIT', Pid, _} ->% restart client if some other
      % process exit
      io:format("Watcher: restart Worker ~p~n",[Worker]),
      watcher(Worker, Low, High);
    finished ->
      io:format("Watcher Finished ~n")
  end.

testRobust(NWorkers, NVictims) ->
  ServerPid = whereis(server),
    if ServerPid == undefined ->
      start_server();
    true ->
      ok
    end,
  server!{range, 1, 15000, NWorkers},
  workerChaos(NVictims,NWorkers).


workerChaos(NVictims,NWorkers) ->
  lists:map(
    fun( _ ) ->
      timer:sleep(500), %% Sleep for .5s
                        %% Choose a random victim
      WorkerNum = rand:uniform(NWorkers),
      io:format("workerChaos killing ~p~n", [workerName(WorkerNum)]), WorkerPid = whereis(workerName(WorkerNum)),
      if %% Check if victim is alive
        WorkerPid == undefined ->
          io:format("workerChaos already dead: ~p~n", [workerName(WorkerNum)]);
        true -> %% Kill Kill Kill
          exit(whereis(workerName(WorkerNum)),chaos)
      end
    end,
    lists:seq( 1, NVictims ) ).

%% hcf x 0 = x
%% hcf x y = hcf y (rem x y)

hcf(X,0) -> X;
hcf(X,Y) -> hcf(Y,X rem Y).

%% relprime x y = hcf x y == 1

relprime(X,Y) ->
  V = hcf(X,Y),
  if
    V == 1
      -> true;
    true
      -> false
  end.

%%euler n = length (filter (relprime n) (mkList n))

euler(N) ->
  RelprimeN = fun(Y) -> relprime(N,Y) end,
  length (lists:filter(RelprimeN,(lists:seq(1,N)))).

%% Take completion timestamp, and print elapsed time

printElapsed(S,US) ->
  {_, S2, US2} = os:timestamp(),
                       %% Adjust Seconds if completion Microsecs > start Microsecs
  if
    US2-US < 0 ->
      S3 = S2-1,
      US3 = US2+1000000;
    true ->
      S3 = S2,
      US3 = US2
  end,
  io:format("Time taken in Secs, MicroSecs ~p ~p~n",[S3-S,US3-US]).


%%sumTotient lower upper = sum (map euler [lower, lower+1 .. upper])

sumTotient(Lower,Upper) ->
  % {_, S, US} = os:timestamp(),
  lists:sum(lists:map(fun euler/1,lists:seq(Lower, Upper))).
  % io:format("Sum of totients: ~p~n", [Res]),
  % printElapsed(S,US).
