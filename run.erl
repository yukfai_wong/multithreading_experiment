#!/usr/bin/env escript
-mode(compile).
% -import(totientrange, [hcf/2, relprime/2, euler/1, sumTotient/2]).
-import(totientrangeNWorkersReliable, [hcf/2,
 	 relprime/2,
	 euler/1,
	 sumTotient/2,
   workerName/1,
   start_server/0,
   server/0,
   server/4,
   totientWorker/0,
   watcher/3,
   workerChaos/2,
   testRobust/4
	]).
%% -*- erlang -*-

main(Args)->
  io:format("Length: ~p~n",[length(Args)]),
  X = lists:map(fun(X) -> list_to_integer(X) end, Args),
  io:format("sumTotient #~w~n", [X]),
  totientrangeNWorkersReliable:testRobust(4,3,1,15000).
