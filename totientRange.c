//author's name: Yuk Fai Wong
//Student ID: 2229695

#include <stdio.h>

// hcf x 0 = x
// hcf x y = hcf y (rem x y)

long hcf(long x, long y)
{
  long t;

  while (y != 0) {
    t = x % y;
    x = y;
    y = t;
  }
  return x;
}


// relprime x y = hcf x y == 1

int relprime(long x, long y)
{
  return hcf(x, y) == 1;
}


// euler n = length (filter (relprime n) [1 .. n-1])

long euler(long n)
{
  long length, i;

  length = 0;
  for (i = 1; i < n; i++)
    if (relprime(n, i))
      length++;
  return length;
}


// sumTotient lower upper = sum (map euler [lower, lower+1 .. upper])

long sumTotient(long lower, long upper)
{
  long i;
  long sum = 0;
  ////////////////////////////////////////////////////////////////////////////////
  //OpenMP
  ////////////////////////////////////////////////////////////////////////////////
  #pragma omp parallel for schedule(dynamic, 1) reduction(+:sum)
    for (i = lower; i <= upper; i++){
      //printf("Thread: %d doing %ld\n", omp_get_thread_num(), i);
      sum = sum + euler(i);
    }
  return sum;
}


int main(int argc, char ** argv)
{
  long lower, upper;

  if (argc != 3) {
    printf("not 2 arguments\n");
    return 1;
  }
  sscanf(argv[1], "%ld", &lower);
  sscanf(argv[2], "%ld", &upper);

  printf("C: Sum of Totients  between [%ld..%ld] is %ld\n", lower, upper, sumTotient(lower, upper));
  return 0;
}
