pre = " 1 "
DS1 = "15000"
DS2 = "30000"
DS3 = "60000"
c = "mainc"
go = "maingo"
sc = "smainc"
sgo = "smaingo"
mult_thread1 = "OMP_NUM_THREADS="
threads = [1,2,4,8,12,16,24,32,48,64]
threads3 = [8, 16, 32, 64]

import os
import subprocess
def run(thread, program, start, end, path = "./", time="time"):
    print(thread+" "+time+ " " + path+ program+ start+ end)
    process = subprocess.check_output([thread+" "+time+ " " + path+ program+ start+ end], shell=True)
    print(process.decode("utf-8"))

print("2.1 Runtimes.")
###############################
print("DS1")
print("sequential")
print("C")
run("", sc, pre, DS1)
print("go")
run("", sgo, pre, DS1)

print("parallel")
print("C")
for i in threads:
	print("core: " + str(i))
	run(mult_thread1+str(i), c, pre, DS1)
print("go")
for i in threads:
	print("core: " + str(i))
	run(mult_thread1+str(i), go, pre, DS1)

################################
print("DS2")
print("C")
run("", sc, pre, DS2)
print("go")
run("", sgo, pre, DS2)

print("parallel")
print("C")
for i in threads:
	print("core: " + str(i))
	run(mult_thread1+str(i), c, pre, DS2)
print("go")
for i in threads:
	print("core: " + str(i))
	run(mult_thread1+str(i), go, pre, DS2)

###############################
print("DS3")
print("parallel")
print("C")
for i in threads3:
	print("core: " + str(i))
	run(mult_thread1+str(i), c, pre, DS3)
print("go")
for i in threads3:
	print("core: " + str(i))
	run(mult_thread1+str(i), go, pre, DS3)
