//author's name: Yuk Fai Wong
//Student ID: 2229695w

package main

import (
  "fmt"
  "os"
  "strconv"
  "time"
  "sync"
)


// Compute the Highest Common Factor, hcf of two numbers x and y
//
// hcf x 0 = x
// hcf x y = hcf y (rem x y)

func hcf(x, y int64) int64 {
  var t int64
  for (y != 0) {
    t = x%y
    x = y
    y = t
  }
  return x
}

// relprime determines whether two numbers x and y are relatively prime
//
// relprime x y = hcf x y == 1

func relprime(x,y int64) bool {
  return hcf(x, y) == 1;
}

// euler(n) computes the Euler totient function, i.e. counts the number of
// positive integers up to n that are relatively prime to n
//
// euler n = length (filter (relprime n) [1 .. n-1])

func euler(n int64) int64 {
  var length, i int64

  length = 0
  for i = 1; i < n; i++ {
    if relprime(n, i) {length++}
  }
  return length
}

// sumTotient lower upper sums the Euler totient values for all numbers
// between "lower" and "upper".
//
// sumTotient lower upper = sum (map euler [lower, lower+1 .. upper])

//each concurrent instances have a private sum to store it result
//they receive work on the `jobs` channel and send the result to results channel after all job have done
func sumTotient(jobs, results chan int64) {
  var sum int64 = 0
  for j := range jobs {
    sum = sum + euler(j)
  }
  results <- sum
  //instances complete unlock
  defer wg.Done()
}

var wg sync.WaitGroup//globla wait lock

//the number of thread(core) control by parameter Threads
func ThreadControl(Threads, lower, upper int64) int64 {
  //define the number of thread need to wait
  jobs := make(chan int64, upper)
  temp_result := make(chan int64, Threads)
  //generate threads and calculate
  for i := int64(0); i<Threads; i++ {
    //add a instances that need to be wait for
    wg.Add(1)
    go sumTotient(jobs, temp_result)
  }

  for i := lower; i <= upper; i++{
    jobs <- i
  }
  close(jobs)//producting job complete
  wg.Wait()//wait all thread to complete
  //collect the return of threadwg
  length := len(temp_result)+1
  var result int64 = 0
  for i := 1; i<length; i++{
    result = result + (<-temp_result)
  }
  return result
}

func main() {
  var lower, upper int64
  var err error
                                             // Read and validate lower and upper arguments
  if len(os.Args) < 3 {
    panic(fmt.Sprintf("Usage: must provide lower and upper range limits as arguments"))
  }

  if lower, err = strconv.ParseInt(os.Args[1],10,64) ; err != nil {
    panic(fmt.Sprintf("Can't parse first argument"))
  }
  if upper, err = strconv.ParseInt(os.Args[2],10,64) ; err != nil {
    panic(fmt.Sprintf("Can't parse second argument"))
  }
                                             // Record start time
  start := time.Now()

  env := os.Getenv("OMP_NUM_THREADS")//load environment value OMP_NUM_THREADS
  threads_num, err := strconv.ParseInt(env, 10, 64)// load the int of OMP_NUM_THREADS
  if  err != nil {
    threads_num = 1//if error occurr, use 1 thread
  }
  // Compute and output sum of totients
  fmt.Println("Sum of Totients between", lower, "and", upper, "is", ThreadControl(threads_num, lower,upper))
  // Record the elapsed time
  t := time.Now()
  elapsed := t.Sub(start)
  fmt.Println("Elapsed time", elapsed)
}
